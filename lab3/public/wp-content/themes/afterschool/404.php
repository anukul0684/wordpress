<?php

    /**
     * 404 template for After School
     */
    
    get_header();
?><section>
    <div id="content">
        <div id="primary">
            
            <article>
            <!-- add content to display on page not found -->
            
            <h1 >404 - Not Found </h1>
            <div style="height:500px;">
                <p> page does not exist</p>
            </div>    
            </article><!-- /article -->
        </div>
        <?php get_sidebar(); ?>
    </div>
</section>
<?php get_footer(); ?>        