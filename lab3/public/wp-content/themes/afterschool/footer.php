<footer>
            <ul>
                <?php wp_nav_menu(['menu' => 'footer-nav']) ?>
            </ul> 
            <p>Copyright &#169; 2015 by After School</p>
        </footer>
    </div>
    <?php wp_footer()?>
</body>
</html>