<div id="secondary">
    <ul>
        <li>
            <a href="<?=get_the_permalink(2); ?>" class="image-link">                
                <?php echo get_the_post_thumbnail(2); ?>
            </a>
            <a href="<?=get_the_permalink(2); ?>" class="page-link">
                Elementary Programs
            </a>
        </li>
        <li>
            <a href="<?=get_the_permalink(17); ?>" class="image-link">
                <?php echo get_the_post_thumbnail(17); ?>
            </a>
            <a href="<?=get_the_permalink(17); ?>" class="page-link">
                Middle School Programs
            </a>
        </li>
        <li>
            <a href="<?=get_the_permalink(19); ?>" class="image-link">
                <?php echo get_the_post_thumbnail(19); ?>
            </a>
            <a href="<?=get_the_permalink(19); ?>" class="page-link">
                High School Programs
            </a>
        </li>
    </ul>
</div>