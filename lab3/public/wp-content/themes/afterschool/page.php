<?php

    /**
     * Page template for After School
     */
    
    get_header();
?><section>
    <div id="content">
        <div id="primary">
            
            <article>
                <!-- show the page name called -->
                <h1 id="single-page-heading"><?php the_title(); ?>
                    <?php if( ($post->ID == 2) || ($post->ID == 17) || ($post->ID == 19) ) : ?>
                        Programs
                    <?php endif; ?>
                </h1>
                
                <?php 
                    //var_dump($post->ID);
                    $page_id = $post->ID;
                    if($page_id == 2) : ?>
                        <div id="page-image" style="display: inline-block; float: left; margin: 3% 3% 2% 1%;">
                            <?php echo get_the_post_thumbnail(1,'medium'); ?>
                        </div> 
                <?php endif; ?>    
                  
                <div id="page-content" style="margin: 0;">             
                    <!-- show the page content -->
                    <?php the_content(); ?>
                </div>
            </article>
        </div>
        <?php get_sidebar(); ?>
    </div>
</section>
<?php get_footer(); ?>        