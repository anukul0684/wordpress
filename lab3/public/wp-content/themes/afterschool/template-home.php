<?php
/**
 * Template Name: Home
 */
    get_header();
 ?><section>
        <div id="content">           
                
            <article id="home-article">
                
                <!-- show the post image -->
                <a href="<?=get_the_permalink(76); ?>" class="home-article-image-link">
                    <?php echo get_the_post_thumbnail(76); ?>
                </a>                    

                <!-- show the post title -->
                <a href="<?=get_the_permalink(76); ?>" class="home-article-page-link">
                    <?=get_the_title(76); ?>
                </a>

                
            </article>
            
            <?php get_sidebar('home'); ?>    
        </div>
    </section>
<?php get_footer(); ?>