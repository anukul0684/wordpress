<?php

    /**
     * Index template for After School
     */
    
    get_header();
?><section>
    <div id="content">
        <div id="primary">
            <h1> News</h1>
            <article>
                <!-- run a loop to list the posts if added through editor -->
				<?php while(have_posts()) : the_post(); ?>

                    <!-- add permalink for each to visit the single detail page -->
                    <h2><a href="<?php the_permalink() ?>" class="post-title"><?php the_title(); ?></a></h2>

                    <!-- add the date on which the post was created -->
                    <p class="posted"><small><?php the_date() ?></small></p>

                    <!-- brief exhibit of the post content -->
                    <p class="small-para">
                        <?=get_the_excerpt(); ?>
                    </p>

                <?php endwhile; ?>                           
                
                <!-- reference - https://wordpress.stackexchange.com/questions/203750/how-to-paginate-this-custom-loop -->
                <div id="pagination">
                    <div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
                    <div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
                </div>

            </article>
        </div>
        <?php get_sidebar(); ?>
    </div>
</section>
<?php get_footer(); ?>        