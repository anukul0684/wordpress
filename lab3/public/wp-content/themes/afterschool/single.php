<?php

    /**
     * Single template for After School
     */
    
    get_header();
?><section>
    <div id="content">
        <div id="primary">
            
            <article>
                <!-- show the post title -->
                <h2 id="single-page-heading"><?php the_title(); ?></h2>    
                
               <!-- add the date on which the post was created -->
               <p class="posted"><small><?php the_date() ?></small></p>
                
                <!-- show the post details -->
                <?php the_content(); ?>
                
                    <?php 
                        //var_dump($post->ID);
                        $page_id = $post->ID;
                        if($page_id != 76) : ?>
                            <div id="comment_form">
                            <!-- /* Display comment template from installed plugin wpDisCuz */ -->
                                <?php 
                                
                                $comments_args = array(
                                    // change the title of send button 
                                    'label_submit'=>'Submit Comment',
                                    // change the title of the reply section
                                    'title_reply'=>'Leave a Reply',
                                    
                                );
                                
                                comments_template($comments_args); ?>
                            </div>
                        <?php endif; ?>
                
            </article>
        </div>
        <?php get_sidebar(); ?>
    </div>
</section>
<?php get_footer(); ?>        