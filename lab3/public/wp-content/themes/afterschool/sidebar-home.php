<div id="secondary-home">
    <ul>
        <li>
            <a href="<?=get_the_permalink(2); ?>" class="image-link-home">                
                <?php echo get_the_post_thumbnail(2); ?>
            </a>
            <a href="<?=get_the_permalink(2); ?>" class="page-link-home">
                <?=get_the_title(2); ?>
            </a>
        </li>
        <li>
            <a href="<?=get_the_permalink(17); ?>" class="image-link-home">
                <?php echo get_the_post_thumbnail(17); ?>
            </a>
            <a href="<?=get_the_permalink(17); ?>" class="page-link-home">
                <?=get_the_title(17); ?>
            </a>
        </li>
        <li>
            <a href="<?=get_the_permalink(19); ?>" class="image-link-home">
                <?php echo get_the_post_thumbnail(19); ?>
            </a>
            <a href="<?=get_the_permalink(19); ?>" class="page-link-home">
                <?=get_the_title(19); ?>
            </a>
        </li>
    </ul>
</div>