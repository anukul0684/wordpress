<?php


/**
 * Enabling features on dashboard and changes on website
 */


register_nav_menus();

/**
 * Add them support for featured images(post-thumbnails)
 */

 add_theme_support('post-thumbnails');

 // reference : https://developer.wordpress.org/reference/functions/comment_form/
//  reorganizing comments form
 if(!function_exists('mo_comment_fields_custom_order')) {

    function mo_comment_fields_custom_order( $fields ) {
        $comment_field = $fields['comment'];
        $author_field = $fields['author'];
        $email_field = $fields['email'];
        $url_field = $fields['url'];
        $cookies_field = $fields['cookies'];
        unset( $fields['comment'] );
        unset( $fields['author'] );
        unset( $fields['email'] );
        unset( $fields['url'] );
        unset( $fields['cookies'] );
        unset( $fields['wp-comment-cookies-consent']);
        // the order of fields is the order below, change it as needed:
        $fields['author'] = $author_field;
        $fields['email'] = $email_field;
        $fields['url'] = $url_field;
        $fields['comment'] = $comment_field;
        $fields['cookies'] = $cookies_field;
        $fields['wp-comment-cookies-consent'] = '';
        // done ordering, now return the fields:
        return $fields;
    }
 }

 add_filter( 'comment_form_fields', 'mo_comment_fields_custom_order' );

 
// best practices for adding css and js links to header or footer
 if(!function_exists('afterschool_scripts')) {
     function afterschool_scripts()
     {
         wp_enqueue_style('afterschool',get_stylesheet_uri(),[],'1.0',false);
     }
 }

 add_action('wp_enqueue_scripts','afterschool_scripts');