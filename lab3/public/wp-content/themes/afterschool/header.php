<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>
    
    <?php wp_head()?>
</head>
<body>
    <div id="wrapper" class="container">
        <header>
            <nav id="top-right-nav">
                <ul>
                    <?php wp_nav_menu(['menu' => 'top-nav']) ?>
                </ul>            
            </nav>
            <img src="http://afterschool.ayntechnologies.com/wp-content/uploads/2020/11/logo.png" alt="logo" />
            <nav id="main-nav">
                <ul>
                    <?php wp_nav_menu(['menu' => 'main-nav']) ?>
                </ul>            
            </nav>
        </header>