<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'afterschool' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G?.c1}l>d]-/-vPdD2MNh4+T#MoV|){JD/dE0j?{~%+-* 10KIz?)}[Ekp+vzTJj');
define('SECURE_AUTH_KEY',  'KPgh+$RW[m/V)&a~baB^=Nz#F:11V:?P*-UD^iC/|B;@{Nfl1j~aD[EQ-Fl_cx>b');
define('LOGGED_IN_KEY',    'AkCD+I1_=AFWy|~+iP6Vs|s>@.g#QKuxuhbje:x@UBm.nBh.i27!1E_ &,k(]~vA');
define('NONCE_KEY',        'r+.~`>p?hf/s^i0QC%D$:OG4EGWSS?@|Gj&|{RU-R9%;de#yju %P;Tyl8Q<mm6e');
define('AUTH_SALT',        'H.={*yZEB|36lFjF|2?qL|Prh9(%s=O 6yCdD||r;-B }(_Hja2|W[j!/LW}wiqc');
define('SECURE_AUTH_SALT', '||a`Q-q|wuJ!=FXh)e%A R1+|5)n!zF/q5^&s$KNt`Ir8jn58|`k6kNOiGH~k(a^');
define('LOGGED_IN_SALT',   'PCF:I/h.GD%,|h_aI&F9|CL~?=EO-dQR~Y[/8B}4}Q|Pyl4,7kx*a3:!kX-jE#y*');
define('NONCE_SALT',       '<TECEvxd8 K<;omcV/3,MV9>{Kki_=sw:H 4%K%+}BY?s[@Jf?e?zO(t|NE:M^jU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_DISPLAY', true );

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
