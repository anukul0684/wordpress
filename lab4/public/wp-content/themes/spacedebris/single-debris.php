<?php

	/**
	 * Single post template for Space Debris
	 */

	get_header();

?>

<div class="row">

	<div id="primary" class="col-xs-12 col-sm-8">
        
        <?php while(have_posts()) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <h2><?php the_debri_title(); ?></a></h2>
            <p><small>Posted on <?php echo get_the_date(); ?></small></p>
            <p><?php the_debri_img() ?></p>
            <p><strong>Description</strong></p>
            <blockquote><?php the_debri_desc() ?></blockquote>
            <p><strong>Size</strong>: <?php the_debri_size() ?></p>   
            <p><strong>Genre</strong>: <?php the_debri_genre() ?></p>            
        <?php endwhile; ?>
		

	</div>

	<?php get_sidebar(); ?>

</div><!-- /row -->


</div><!-- /CONTENT -->

<?php get_footer(); ?>
