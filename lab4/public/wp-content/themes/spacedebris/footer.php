
<?php

	/**
	 * Footer for Space Debris
	 */

?><!-- FOOTER -->

<div class="container-fluid">

	<div class="row">

		<div id="footer" class="col-xs-12">

			<p>Contents copyright &copy; 2015 by Space Debris.</p>

		</div><!-- /footer -->

	</div>


</div><!-- /FOOTER -->


<script></script>

<?php wp_footer(); ?>
</body>
</html>