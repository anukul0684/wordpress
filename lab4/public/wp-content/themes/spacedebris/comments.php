<?php

    /**
	 * comments template for Space Debris
	 */
    
?><div id="comments">
    <div id="comment_form">
        <?php comment_form(); ?>
    </div>
    <div id="comment_list">
        <?php wp_list_comments(); ?>
    </div>
</div>