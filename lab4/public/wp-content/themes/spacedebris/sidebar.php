<?php 

	/**
	 * sidebar for Space Debris
	 */

?><div id="secondary" class="col-xs-12 col-sm-4">
		<?php $featured_pages = get_pages(['meta_key' => 'featured', 'meta_value' => '1']); ?>

		<?php foreach($featured_pages as $page) : ?>
		<?php //var_dump($featured_pages); ?>
			<div class="callout col-xs-12">
				<h2><a href="<?=get_the_permalink($page->ID)?>"><?=get_the_title($page->ID);?></a></h2>
				<a href="<?=get_the_permalink($page->ID)?>">
					<?=get_the_post_thumbnail($page->ID)?>					
				</a>
			</div>
		<?php endforeach;?>
		

	</div>