<?php

    /**
	 * Search template for Space Debris
	 */

	get_header();

?><div class="row">

	<div id="primary" class="col-xs-12 col-sm-8">
        <h1> You have search for <?php the_search_query(); ?> </h1>
        <h3> returned <?=count($posts)?> result(s) </h3>
        <?php if($posts): ?>
            <?php while(have_posts()) : the_post(); ?>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p><small>Posted on <?php the_date() ?></small></p>
                <?php the_excerpt(); ?>
            <?php endwhile; ?>
        <?php else: ?>
            <h3>Try again.</h3>
            <div class="search_box">
                <?php get_search_form(); ?>
            </div>
        <?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

</div><!-- /row -->


</div><!-- /CONTENT -->

<?php get_footer(); ?>
