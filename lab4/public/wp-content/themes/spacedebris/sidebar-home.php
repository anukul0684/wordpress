<?php 

    /**
	 * Sidebar for home page for Space Debris
	 */

?><div id="callouts" class="row">
    <?php $pages=get_pages(['meta_key' => 'featured', 'meta_value' => '1']); ?>

    <?php foreach($pages as $page) : ?>
        <div class="callout col-xs-12 col-sm-4">
            <h2>
                <a id="title-link" href="<?=get_the_permalink($page->ID)?>" title="click to view more">
                    <?=get_the_title($page->ID); ?>
                </a>
            </h2>
            <a href="<?=get_the_permalink($page->ID)?>" title="click to view more">
                <?=get_the_post_thumbnail($page->ID)?>
            </a>
            <p><?=get_the_excerpt($page->ID)?></p>
            <a class="btn btn-primary pull-right" href="<?=get_the_permalink($page->ID)?>">Read More &gt;&gt;</a>
        </div>
    <?php endforeach; ?>
	

</div><!-- /row -->