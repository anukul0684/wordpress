<?php

	/**
	 * 404 template for Space Debris
	 */

	get_header();

?><div class="row">

		<div id="primary" class="col-xs-12 col-sm-8">
			<h1> 404 Not Found </h1>
			<p>Page not found. Please use navigation for view or search box below to find what you seek.</p>
			<div class="search_box">
				<?php get_search_form(); ?>
			</div>

		</div>

		<?php get_sidebar(); ?>

	</div><!-- /row -->


</div><!-- /CONTENT -->

<?php get_footer(); ?>
