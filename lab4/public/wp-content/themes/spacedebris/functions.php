<?php

    /**
	 * Enabling functions for Space Debris
	 */



if(!function_exists('spacedebris_scripts'))
{
    function spacedebris_scripts()
    {

        wp_enqueue_style('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
                            [],'3.3.2',false);

        wp_enqueue_style('spacedebris',get_stylesheet_uri(),['bootstrap'], '1.0', false);

        wp_enqueue_script('spacedebris',get_template_directory_uri() . '/js/spacedebris.js',
                            ['jquery'],'1.0',true);

        // wp_enqueue_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js?ver=2.1.3',
        //                     [],'2.1.3',false);
    }

    add_action('wp_enqueue_scripts','spacedebris_scripts');
}

//Shortcodes
if(!function_exists('wdd_hello')) {

    function wdd_hello($atts,$content='')
    {
        // $out = '';
        // $out .= '<pre>';
        // $out .= print_r($atts,true);
        // $out .= print_r($content,true);
        // $out .='</pre>';

        //$out = "<div class='{$atts['class']}'>{$atts['msg']}</div>";

        $class = $atts['class'] ?? '';
        $style = $atts['style'] ?? '';
        $msg = $atts['msg'] ?? '';
        $out = "<div class='{$class}' style='{$style}'>{$msg}</div>";
        return $out;
    }

    // first param is the name of our shortcode the use will write
    // second param is the name of the function that executes out shortcode
    add_shortcode('wddhello','wdd_hello');
}

if(!function_exists('wdd_bq')) {

    function wdd_bq($atts,$content='')
    {
        return "<blockquote class=\"blockquote\">{$content}</blockquote>";
    }

    add_shortcode('wddbq', 'wdd_bq');
}

if(!function_exists('i_really_like_cheerios')) {
    function i_really_like_cheerios($atts,$content='')
    {
        $link = $atts['link'] ?? '#';
        $out = "<div style=\"padding:20px 0; font-size:1.6rem;\">
                    <p class=\"text-center\">
                        <a class=\"btn btn-lg btn-success\" href=\"{$link}\">
                            {$content}

                        </a>
                    </p>
                </div>";
        return $out;
    }

    add_shortcode('cta','i_really_like_cheerios');
}

if(!function_exists('debris_custom_post_type')) {
    function debris_custom_post_type()
    {
        // labels (singular) - array
        // public - boolean
        // capability_type - post or page
        // has_archive - boolean
        // heirarchical - boolean
        // taxonomies - array eg.: ['category','tag']
        // supports - array
        $params = array(
            'labels' => [
                'name' => 'Debris',
                'singular_name' => 'Item'
            ],
            'public' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => true,
            'taxonomies' => ['category'],
            'supports' => [
                'title',
                'editor',
                'excerpt',
                //'custom-fields',
                'revisions',
                'page-attributes',
                'post-thumbnails'
            ]
        );
        register_post_type('Debris',$params);
    }
    add_action('init','debris_custom_post_type');

    //flush_rewrite_rules(true);
}

if(!function_exists('the_debri_title')) {
    function the_debri_title()
    {
        global $post;
        $name = get_post_meta($post->ID,'debri_name',true);
        echo esc_html($name);
    }

}

if(!function_exists('the_debri_img')) {
    function the_debri_img()
    {
        global $post;
        // $src = esc_html(get_post_meta($post->ID,'debri_image',true));
        // $img = '';
        // if($src) {
        //     $img = "<img src=\"{$src}\" alt=\"Debri image\">";
        // }
        // echo $img;
        $id = get_post_meta($post->ID, 'debri_image',true);
        echo wp_get_attachment_image($id,'full');
    }

}

if(!function_exists('the_debri_size')) {
    function the_debri_size()
    {
        global $post;
        $size = get_post_meta($post->ID,'debri_size',true);
        echo esc_html($size);
    }

}

if(!function_exists('the_debri_desc')) {
    function the_debri_desc()
    {
        global $post;
        $desc = get_post_meta($post->ID,'debri_description',true);
        echo esc_html($desc);
    }

}

if(!function_exists('the_debri_genre')) {
    function the_debri_genre()
    {
        global $post;
        $genre = get_post_meta($post->ID,'debri_genre',true);
        echo esc_html($genre);
    }

}

// if(!function_exists('spacedebris_protect_users_api_endpoint')) {
//     function spacedebris_protect_users_api_endpoint()
//     {
//         unset($endpoints['/wp/v2/users']);
//         return $endpoints;
//     }
// }

// add_filter('rest_endpoints','spacedebris_protect_users_api_endpoint');