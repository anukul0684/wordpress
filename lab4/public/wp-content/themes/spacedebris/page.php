<?php

	/**
	 * Page view template for Space Debris
	 */

	get_header();

?><div class="row">

	<div id="primary" class="col-xs-12 col-sm-8">
		<h1> <?php the_title(); ?></h1>
			
		<?php the_content(); ?>
		
	</div>

	<?php get_sidebar(); ?>

</div><!-- /row -->


</div><!-- /CONTENT -->

<?php get_footer(); ?>
