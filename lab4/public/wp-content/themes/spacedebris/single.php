<?php

	/**
	 * Single post template for Space Debris
	 */

	get_header();

?>

<div class="row">

	<div id="primary" class="col-xs-12 col-sm-8">
		<h1><?php the_title(); ?></a></h1>
        <p><small>Posted on <?php echo get_the_date(); ?></small></p>
        <?php the_content(); ?>
		<?php comments_template(); ?>

	</div>

	<?php get_sidebar(); ?>

</div><!-- /row -->


</div><!-- /CONTENT -->

<?php get_footer(); ?>
