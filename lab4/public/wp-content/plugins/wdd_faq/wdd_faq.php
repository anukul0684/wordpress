<?php

/**
 * Plugin Name: WDD FAQs
 * Description: a simple FAQ
 * Author: Anu Kulshrestha
 * Author URI: http://space.ayntechnologies.com
 * Plugin URI: http://space.ayntechnologies.com/wordpress/plugins/wdd_faq/wdd_faq
 * Description: Settings common to all WDD themes... 
 * Version: 1.0
 */

if(!function_exists('wdd_faq_scripts')) {
    function wdd_faq_scripts()
    {
        wp_enqueue_style('wdd-faq',plugin_dir_url(__FILE__).'css/wdd_faq.css',
                        ['bootstrap'],'1.0',false);
        wp_enqueue_script('wdd-faq',plugin_dir_url(__FILE__). 'js/wdd_faq.js',
                        ['jquery'],'1.0',true);
    }
    add_action('wp_enqueue_scripts','wdd_faq_scripts');
}

if(!function_exists('wdd_faq')) {
    function wdd_faq($atts,$content='')
    {
        $question=$atts['q'];
        $out = "<div class=\"wdd_faq\">

                    <p class=\"wdd_faq_q\" >
                        {$question}
                    </p>
            
                    <div class=\"wdd_faq_a\">
                        <!-- <strong>A.</strong> -->
                        {$content}
            
                    </div>
        
                </div>";
        return $out;
    }

    add_shortcode('faq','wdd_faq');
}