<?php

/**
 * Plugin Name: WDD Theme Settings
 * Author: Anu Kulshrestha
 * Author URI: http://space.ayntechnologies.com
 * Plugin URI: http://space.ayntechnologies.com/wordpress/plugins/theme_settings
 * Description: Settings common to all WDD themes... 
 * Version: 1.0
 */

if(!function_exists('wdd_config_theme')) {

    /**
     * 
     */
    function wdd_config_theme()
    {
        register_nav_menus();

        add_theme_support('post-thumbnails');
    }

    add_action('init', 'wdd_config_theme');
}

if(!function_exists('wdd_echo_note')) {
    /**
     * 
     */
    function wdd_echo_note()
    {
        echo "\n" . "<!--This Theme uses WDD Theme Settings -->";
    }

    add_action('wp_head', 'wdd_echo_note', 10);
}

if(!function_exists('spacedebris_title')) {
    function spacedebris_title($title)
    {
        return strtoupper($title);
    }

    add_filter('the_title','spacedebris_title');
}

if(!function_exists('spacedebris_content')) {
    function spacedebris_content($content) 
    {
        return '<div style="color:#f00">' . $content . '</div>';
    }

    //add_filter('the_content','spacedebris_content');
}


