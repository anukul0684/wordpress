<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'space_debris' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w&ZYw-*/AgY%h0c1&=I+VJ<zFAz|x$XdliX~K_j1bSOgs94:|K~qqf}`mPSQu77|');
define('SECURE_AUTH_KEY',  '=4{wPnB;67`GM+PLR{gmt!z<?1CEzIoq!-<NL&)f`lX3D_C~nmT0[D%~3%%bOtXN');
define('LOGGED_IN_KEY',    'y$TF3F20]`j)-7,<[B)KBOF..NPaPS+@F-H#-#lZg3:L-V:j.#Y7Kmc}mpXi[7CH');
define('NONCE_KEY',        ';6tf`OzL+I?l0s)U>]x`ynOD9%kX,3&ruT1lF6PUs!-244}<byrI_:|3KYNRV+GI');
define('AUTH_SALT',        'sfYsd*B|QjfH-%WkVxuP+($= 8?fy`HnSo{qt3m9k|_|bz`196ms}v(Yx$7i7|X/');
define('SECURE_AUTH_SALT', 'W T&Ub`#F~8wTsd%i^|N_R@5:5i6jKHTbT.3S_>*-JR[U8T$cVmqNyRE,F m,-n|');
define('LOGGED_IN_SALT',   ',A#KHA`3?|qpVNB&%~b%I(HHi<X!+nK#NffpHlzNnw}_t8kno?dr;K<|?li|+?/q');
define('NONCE_SALT',       'b&IL4} JR- +$tnQ`4/1H,(duU n&@7`<</rBK]N> &0q$O=nz@f)EhxCq}OjsY5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
