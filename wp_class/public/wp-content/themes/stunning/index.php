<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stunning
 */

get_header();
$GLOBALS['firstPosts']=array();
?>
<div id="content" class="site-content global-layout-no-sidebar">

    <?php
     if ( stunning_get_option( 'show_sliderposts' ) ) { 
        do_action('stunning_slider_action');

         
     }


    ?>

    



    <?php
    do_action('stunning_otherspost_action');
    
    
    ?>


 




    <div class="rpl-lg-3" id="sidebar-secondary">

        <?php get_sidebar() ?>

    </div>
</div>

</div>
</div>

<?php get_footer();?>