<?php
function stunning_scripts() {
	global $stunning_theme_options;
	/*body  */
    wp_enqueue_style('stunning_google_lora_body', '//fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap', array(), null);
    /*heading  */
    wp_enqueue_style('stunning_google_prata_heading', '//fonts.googleapis.com/css2?family=Prata&display=swap', array(), null);

	wp_enqueue_style( 'stunning_allmin', get_template_directory_uri().'/assets/css/all.min.css', array() );
	wp_enqueue_style( 'slick', get_template_directory_uri().'/assets/css/slick.css', array() );
	wp_enqueue_style( 'slick-theme', get_template_directory_uri().'/assets/css/slick-theme.css', array() );



	wp_enqueue_style( 'stunning-style', get_stylesheet_uri(), array(), _S_VERSION );

	wp_style_add_data( 'stunning-style-rtl', 'rtl', 'replace' );

	wp_enqueue_script( 'stunning_allmin', get_template_directory_uri() . '/assets/js/all.min.js', array('jquery'), _S_VERSION, true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', array('jquery'), _S_VERSION, true );

	wp_enqueue_script( 'stunning-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'stunning_script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'stunning_scripts' );