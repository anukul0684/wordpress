<?php

$wp_customize->add_section('stunning_subscribe_setting',array(
    'title'=>esc_html__( 'Subscribe options', 'stunning' ),
    'panel'      => 'stunning_options_panal',
    
    'priority'=>'31'    
));



     $wp_customize->add_setting( 'stunning_subscribe_shortcode',
        array(
            'default'           => $default['stunning_subscribe_shortcode'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
        );

        $wp_customize->add_control( 'stunning_subscribe_shortcode',
        array(
            'label'    => esc_html__( 'Subscribe form shortcode', 'stunning' ),
            'section'  => 'stunning_subscribe_setting',
            'type'     => 'text',
            'priority' => 40,
            'settings'  => 'stunning_subscribe_shortcode', // Setting ID
        )
        );