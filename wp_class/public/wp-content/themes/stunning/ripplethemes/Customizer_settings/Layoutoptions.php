<?php
    // Layout Section.

    $wp_customize->add_section('stunning_layout_options',array(
        'title'=>esc_html__( 'Layout Options', 'stunning' ),
        'description'=>esc_html__( 'Layout Options', 'stunning'),
        'priority'=>'30',
        'panel'      => 'stunning_options_panal',
    ));

    $wp_customize->add_setting(
        'stunning_body_background',
        array(
           'default'   => $default['stunning_body_background'], // Set default value
           'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
           )
    );
   
    $wp_customize->add_control(
        new WP_Customize_Control(
               $wp_customize,
               'stunning_body_background', // Setting ID
               array(
                   'label'     => __('Remove post background and sidebar box shadow', 'stunning'),
                   'section'   => 'stunning_layout_options', // No hyphen
                   'settings'  => 'stunning_body_background', // Setting ID
                   'type'      => 'checkbox',
               )
           )
    );


