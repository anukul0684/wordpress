<?php


$wp_customize->add_section('stunning_post_setting',array(
        'title'=>esc_html__( 'Post Settings', 'stunning' ),
        'panel'      => 'stunning_options_panal',
        
        'priority'=>'30'    
    ));
     $wp_customize->add_setting(
         'stunning_post_show_author',
         array(
            'default'   => $default['stunning_post_show_author'], // Set default value
            'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
            )
     );
    
     $wp_customize->add_control(
         new WP_Customize_Control(
                $wp_customize,
                'stunning_post_show_author', // Setting ID
                array(
                    'label'     => __('Show Author', 'stunning'),
                    'section'   => 'stunning_post_setting', // No hyphen
                    'settings'  => 'stunning_post_show_author', // Setting ID
                    'type'      => 'checkbox',
                )
            )
     );


     $wp_customize->add_setting(
        'stunning_post_show_date',
        array(
           'default'            => $default['stunning_post_show_date'], // Set default value
           'sanitize_callback'  => 'stunning_sanitize_checkbox', // Sanitize input
           )
    );
   
    $wp_customize->add_control(
        new WP_Customize_Control(
               $wp_customize,
               'stunning_post_show_date', // Setting ID
               array(
                   'label'     => __('Show Posted Date', 'stunning'),
                   'section'   => 'stunning_post_setting', // No hyphen
                   'settings'  => 'stunning_post_show_date', // Setting ID
                   'type'      => 'checkbox',
               )
           )
    );


           // Setting Read More Text.
        $wp_customize->add_setting( 'stunning_readmore_text',
        array(
            'default'           => $default['stunning_readmore_text'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
        );

        $wp_customize->add_control( 'stunning_readmore_text',
        array(
            'label'    => esc_html__( 'Read more button text', 'stunning' ),
            'section'  => 'stunning_post_setting',
            'type'     => 'text',
            'priority' => 40,
            'settings'  => 'stunning_readmore_text', // Setting ID
        )
        );




      $wp_customize->add_setting(
        'stunning_post_next_post_link',
        array(
           'default'   => $default['stunning_post_next_post_link'], // Set default value
           'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
           )
    );
   
    $wp_customize->add_control(
        new WP_Customize_Control(
               $wp_customize,
               'stunning_post_next_post_link', // Setting ID
               array(
                   'label'     => __('Next/Previous Post Link', 'stunning'),
                   'section'   => 'stunning_post_setting', // No hyphen
                   'settings'  => 'stunning_post_next_post_link', // Setting ID
                   'type'      => 'checkbox',
               )
           )
    );

