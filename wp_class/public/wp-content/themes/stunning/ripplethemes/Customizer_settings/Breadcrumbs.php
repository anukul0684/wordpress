<?php


$wp_customize->add_section('stunning_breadcrumbs',array(
    'title'=>esc_html__( 'Breadcrumbs', 'stunning' ),
    'panel'      => 'stunning_options_panal',
    
    'priority'=>'30'    
));

$wp_customize->add_setting(
    'stunning_show_breadcrumbs',
    array(
       'default'   => $default['stunning_show_breadcrumbs'], // Set default value
       'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
       )
);

$wp_customize->add_control(
    new WP_Customize_Control(
           $wp_customize,
           'stunning_show_breadcrumbs', // Setting ID
           array(
               'label'     => __('Show Breadcrumbs', 'stunning'),
               'section'   => 'stunning_breadcrumbs', // No hyphen
               'settings'  => 'stunning_show_breadcrumbs', // Setting ID
               'type'      => 'checkbox',
           )
       )
);
