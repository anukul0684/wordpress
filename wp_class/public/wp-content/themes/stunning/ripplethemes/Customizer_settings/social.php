<?php
    // Layout Section.

    $wp_customize->add_section('stunning_my_social',array(
        'title'=>esc_html__( 'Social', 'stunning' ),
        'description'=>esc_html__( 'Social Links', 'stunning'),
        'priority'=>'30',
        'panel'      => 'stunning_options_panal',

    ));
    
        // facebook
    $wp_customize->add_setting( 'facebook_social', array(
        'capability' => 'edit_theme_options',
        'default' => $default['facebook_social'],
        'sanitize_callback' => 'sanitize_text_field',
      ) );
      
      $wp_customize->add_control( 'facebook_social', array(
        'type' => 'text',
        'section' => 'stunning_my_social', // Add a default or your own section
        'settings'=>'facebook_social',
        'description' => __( 'Facebook','stunning' ),
      ) );
      

        // twitter
        $wp_customize->add_setting( 'twitter_social', array(
            'capability' => 'edit_theme_options',
            'default' => $default['twitter_social'],
            'sanitize_callback' => 'sanitize_text_field',
          ) );
          
          $wp_customize->add_control( 'twitter_social', array(
            'type' => 'text',
            'section' => 'stunning_my_social', // Add a default or your own section
            'settings'=>'twitter_social',
            'description' => __( 'Twitter','stunning' ),
          ) );

    
        // google
        $wp_customize->add_setting( 'google_social', array(
            'capability' => 'edit_theme_options',
            'default' => $default['google_social'],
            'sanitize_callback' => 'sanitize_text_field',
          ) );
          
          $wp_customize->add_control( 'google_social', array(
            'type' => 'text',
            'section' => 'stunning_my_social', // Add a default or your own section
            'settings'=>'google_social',
            'description' => __( 'Google','stunning' ),
          ) );
   
    