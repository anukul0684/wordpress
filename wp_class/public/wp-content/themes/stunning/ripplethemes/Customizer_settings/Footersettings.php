<?php
    // Layout Section.

    $wp_customize->add_section('stunning_footer_options',array(
        'title'=>esc_html__( 'Footer Options', 'stunning' ),
        'description'=>esc_html__( 'Footer options', 'stunning'),
        'priority'=>'30',
        'panel'      => 'stunning_options_panal',
    ));



        
        $wp_customize->add_setting( 'stunning_footer_copyright',
        array(
            'default'           => $default['stunning_footer_copyright'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wp_kses_post',
        )
        );

        $wp_customize->add_control( 'stunning_footer_copyright',
        array(
            'label'    => esc_html__( 'Copyright Text', 'stunning' ),
            'section'  => 'stunning_footer_options',
            'type'     => 'textarea',
            'priority' => 40,
            'settings'  => 'stunning_footer_copyright', // Setting ID

        )
        );

          
            $wp_customize->add_setting( 'stunning_footer_poweredby',
            array(
                'default'           => $default['stunning_footer_poweredby'],
                'capability'        => 'edit_theme_options',
                'sanitize_callback' => 'wp_kses_post',
            )
            );
    
            $wp_customize->add_control( 'stunning_footer_poweredby',
            array(
                'label'    => esc_html__( 'Powered By Text', 'stunning' ),
                'section'  => 'stunning_footer_options',
                'type'     => 'textarea',
                'priority' => 40,
                'settings'  => 'stunning_footer_poweredby', // Setting ID
    
            )
            );



            $wp_customize->add_setting(
                'show_logo_in_footer',
                array(
                   'default'   => $default['show_logo_in_footer'], // Set default value
                   'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
                   )
            );
           
            $wp_customize->add_control(
                new WP_Customize_Control(
                       $wp_customize,
                       'show_logo_in_footer', // Setting ID
                       array(
                           'label'     => __('Show logo in footer', 'stunning'),
                           'section'   => 'stunning_footer_options', // No hyphen
                           'settings'  => 'show_logo_in_footer', // Setting ID
                           'type'      => 'checkbox',
                       )
                   )
            );

