<?php
    // Layout Section.

    $wp_customize->add_section('stunning_myownoptions',array(
        'title'=>esc_html__( 'Slider Posts', 'stunning' ),
        'description'=>esc_html__( 'Your options are here', 'stunning'),
        'priority'=>'30',
        'panel'      => 'stunning_options_panal',
    ));



        $wp_customize->add_setting( 'show_sliderposts', array(
            'default'   => $default['show_sliderposts'], // Set default value
            'sanitize_callback' => 'stunning_sanitize_checkbox', // Sanitize input
            )
        );
    
        $wp_customize->add_control( 
            new WP_Customize_Control(
                $wp_customize,
                'show_sliderposts', // Setting ID
                array(
                    'label'     => __( 'Show Slider Posts', 'stunning' ),
                    'section'   => 'stunning_myownoptions', // No hyphen
                    'settings'  => 'show_sliderposts', // Setting ID
                    'type'      => 'checkbox',
                )
            )
        );




       /* Featuredpost cat selection */
    $wp_customize->add_setting( 'stunning-slider-category',
     array(
                'capability'        => 'edit_theme_options',
                'default'           => $default['stunning-slider-category'],
                'sanitize_callback' => 'absint',
          ) );

/*Featuredpost Category Selection*/
    $wp_customize->add_control(
        new Stunning_Customize_Category_Dropdown_Control(
            $wp_customize,
    'stunning-slider-category',
            array(
                    'label'     => __( 'Select Category', 'stunning' ),
                    'section'   => 'stunning_myownoptions',
                    'settings'  => 'stunning-slider-category',
                    'type'      => 'category_dropdown',
                    'priority'  => 0
                 )
        )
        
    );

        $wp_customize->add_setting( 'stunning-slider-no-posts',
        array(
               'capability'        => 'edit_theme_options',
               'default'           => $default['stunning-slider-no-posts'],
               'sanitize_callback' => 'absint',
         ) );

         $wp_customize->add_control( 'stunning-slider-no-posts', array(
            'type' => 'text',
            'section' => 'stunning_myownoptions', 
            'settings'  => 'stunning-slider-no-posts',
           
            'description' => __( 'No. of posts','stunning' ),
          ) );

