<?php
    /* Theme Options Panel */
    $wp_customize->add_panel( 'stunning_options_panal', array(
        'priority' => 30,
        'capability' => 'edit_theme_options',
        'title' => __( 'Stunning Theme Options', 'stunning' ),
    ) );

    
    

require get_template_directory().'/ripplethemes/Customizer_settings/Sanitize_functions.php';

// featured posts
require get_template_directory().'/ripplethemes/Customizer_settings/sliderpost.php';

// social links
require get_template_directory().'/ripplethemes/Customizer_settings/social.php';


// post settings
require get_template_directory().'/ripplethemes/Customizer_settings/Postsetting.php';

// Breadcrumbs setting
require get_template_directory().'/ripplethemes/Customizer_settings/Breadcrumbs.php';

// subscribe
require get_template_directory().'/ripplethemes/Customizer_settings/subscribe_options.php';


// Canvas Slider
require get_template_directory().'/ripplethemes/Customizer_settings/Canvas_slider.php';

// Footer
require get_template_directory().'/ripplethemes/Customizer_settings/Footersettings.php';

// related Posts
require get_template_directory().'/ripplethemes/Customizer_settings/Single_page_settings.php';

// pro button
require get_template_directory().'/ripplethemes/Customizer_settings/pro-button.php';

// layout
require get_template_directory().'/ripplethemes/Customizer_settings/Layoutoptions.php';











