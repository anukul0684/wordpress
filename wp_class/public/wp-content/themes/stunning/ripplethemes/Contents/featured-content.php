<div class="rpl-lg-4 <?php echo get_the_ID() ?>">
    <article class="feature-post">
        <figure class="entry-thumb aligncenter">
        
              <?php 
              the_post_thumbnail( 'large' ) 
              ?>
        </figure>
        <div class="entry-content">
            
                <?php
                
                            if(stunning_get_option('stunning_featured_show_cat')){?>
                            <span class="category is-center">
                                <a href="">
                                <?php
                                echo esc_html(get_the_category()[0]->cat_name);
                                ?>
                                </a>
                                </span>
                                <?php

                            }
                            ?>
           
            <h4><a href="<?php echo esc_url(get_permalink()) ?>"><?php echo esc_html(get_the_title()) ?> </a></h4>

        </div>
    </article>
</div>