<?php
$url = get_the_post_thumbnail_url(get_the_ID(),'large');
 ?>

<article  id="post-<?php the_ID(); ?>" <?php post_class('hero-article');?> style=  "background-image: url('<?php echo esc_url($url) ?> ')" ? >
    <div class="slider-caption is-center">
        <div class="caption-content">
            <span class="category is-center">
                <a href="#">
                    <?php
                        echo esc_html(get_the_category()[0]->cat_name);
                    ?>
                </a>
            </span>
            <div class="entry-content">
                <h2><a href=<?php echo esc_url(get_permalink()) ?>"><?php echo esc_html(get_the_title()) ?></a></h2>
            </div>
            <div class="entry-meta">
            <?php if (stunning_get_option('stunning_post_show_date')&& 'post' === get_post_type()):
                      
                      stunning_posted_on() ;

                      endif
                          ?>

                    <!-- Author -->

                    <?php if (stunning_get_option('stunning_post_show_author')&& 'post' === get_post_type()){ ?>
                    <?php 
                        stunning_posted_by();
                        
                    ?>
                    <?php

                            }
                        ?>

                    <span class="comments-link">
                        <?php $cmt_link = get_comments_link(); 
                                                $num_comments = get_comments_number();
                                                if ( $num_comments == 0 ) {
                                                    $stunning_comment = __( 'No Comments', 'stunning' );
                                                } elseif ( $num_comments > 1 ) {
                                                    $stunning_comment = $num_comments . __( ' Comments', 'stunning' );
                                                } else {
                                                    $stunning_comment = __('1 Comment', 'stunning' );
                                                }
                                            ?>
                        <a href="<?php echo esc_url( $cmt_link ); ?>"><?php echo esc_html( $stunning_comment );?></a>
                    </span>
                
            </div>
        </div>

    </div>
</article>