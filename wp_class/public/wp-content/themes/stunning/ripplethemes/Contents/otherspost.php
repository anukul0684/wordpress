<?php
$readmore=  stunning_get_option('stunning_readmore_text');

?>



<div class="rpl-xl-6">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( has_post_thumbnail() ) : ?>
        <figure class="entry-thumb aligncenter">
            <a href="<?php echo esc_url(get_permalink()) ?>">
            <?php the_post_thumbnail( );?>
            </a>
        </figure>
    <?php endif ?>

        <div class="post-wrapper">
            <?php do_action( 'stunning_social_sharing' ,get_the_ID() );?>
            <div class="main-entry-content">
                <span class="category is-start">

                    <?php
                            $category = get_the_category();
                        if  (isset($category[0])) {
                            echo   '<a href="' . esc_url(get_category_link($category[0]->term_id)) . '">' . esc_html($category[0]->cat_name) . '</a>';
                        } ?>
                </span>
                <div class="entry-header">
                            <?php
                                if ( is_singular() ) :
                                    the_title( '<h1 class="entry-title">', '</h1>' );
                                else :
                                    the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                                endif;
                             ?>
                </div>

                <div class="entry-meta">
                    <!-- Date -->

                    <?php if (stunning_get_option('stunning_post_show_date')&& 'post' === get_post_type()):
                      
                      stunning_posted_on() ;

                      endif
                          ?>

                    <!-- Author -->

                    <?php if (stunning_get_option('stunning_post_show_author')&& 'post' === get_post_type()){ ?>
                    <?php 
                        stunning_posted_by();
                        
                    ?>
                    <?php

                            }
                        ?>

                    <span class="comments-link">
                        <?php $cmt_link = get_comments_link(); 
                                                $num_comments = get_comments_number();
                                                if ( $num_comments == 0 ) {
                                                    $stunning_comment = __( 'No Comments', 'stunning' );
                                                } elseif ( $num_comments > 1 ) {
                                                    $stunning_comment = $num_comments . __( ' Comments', 'stunning' );
                                                } else {
                                                    $stunning_comment = __('1 Comment', 'stunning' );
                                                }
                                            ?>
                        <a href="<?php echo esc_url( $cmt_link ); ?>"><?php echo esc_html( $stunning_comment );?></a>
                    </span>


                </div>
                <div class="entry-content">
                    <p><?php  the_excerpt( );?> </p>
                </div>
                <?php
                if(!empty($readmore)) {
                ?>
                <div class="readMore"><a href="<?php the_permalink(); ?>"
                        class="common-button is-border"><?php echo esc_html($readmore); ?></a></div>
                <?php  } ?>
            </div>
        </div>
    </article>
</div>