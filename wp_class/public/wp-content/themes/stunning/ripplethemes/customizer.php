<?php
/**
 * stunning Theme Customizer
 *
 * @package stunning
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'stunning_default_theme_options' ) ) :

	/**
	 * Get default theme options.
	 *
	 * @since 1.0.0
	 *
	 * @return array Default theme options.
	 */
	function stunning_default_theme_options() {

		$defaults                                 = array();
		
		//Top header

		// top header social links
		$defaults['facebook_social']             = 'https://www.facebook.com/';
		$defaults['twitter_social']             = 'https://www.facebook.com/';
		$defaults['google_social']         = 'https://www.facebook.com/';

		$defaults['stunning_subscribe_shortcode']        = esc_html( '' );


	

	



		//canvas sidebar
		$defaults['stunning_canvas_aboutme_title']        = esc_html__( 'About Me', 'stunning' );
		$defaults['stunning_canvas_Author_Name']        	= esc_html__( 'John Cena', 'stunning' );
		$defaults['stunning_canvas_Author_Desc']  	  	= esc_html__( 'Hello word I am stunning Guy', 'stunning' );

		//canvas sidebar social share
		$defaults['stunning_canvas_Author_facebook']  	  	='https://www.facebook.com/';
		$defaults['stunning_canvas_Author_twitter']  	  	= 'https://www.twitter.com/';
		$defaults['stunning_canvas_Author_linkedin']  	  	= 'https://www.linkedin.com/';
		$defaults['stunning_canvas_Author_youtube']  	  	= 'https://www.youtube.com/';

		
		// slider posts
		$defaults['show_sliderposts']			= 1;
		$defaults['stunning-slider-category']			= 1;
		$defaults['stunning-slider-no-posts']			= 4;

		// post options
		$defaults['stunning_post_show_author']			= 1;
		$defaults['stunning_post_show_date']				= 1;
		$defaults['stunning_post_next_post_link']			= 1;
		$defaults['stunning_readmore_text']        		= esc_html__( 'Read more', 'stunning' );

		
		// Related Posts
		$defaults['you_may_like_title']             = esc_html__( 'Related Post', 'stunning' );
		$defaults['stunning_show_related']             = 1;

		// Breadcrumbs
		$defaults['stunning_show_breadcrumbs']			= 1;

		// layout
		$defaults['stunning_body_background']			= 0;

		

		//copyright
		$defaults['stunning_footer_copyright']        		=  __('<p>Copyright © 2020  All Rights Reserved.</p>','stunning');
			
		//powered by

		$defaults['stunning_footer_poweredby']        		=  __('<p>Powered by : Ripplethemes </p>','stunning');
		$defaults['show_logo_in_footer']        		=  '1';

		


		
		return $defaults;
	}

endif;



if ( ! function_exists( 'stunning_get_option' ) ) :

	/**
	 * Get theme option.
	 * @param string $key Option key.
	 * @return mixed Option value.
	 */
	function stunning_get_option( $key ) {

		if ( empty( $key ) ) {

			return;

		}

		$value            = '';

		$default          = stunning_default_theme_options();

		$default_value    = null;

		if ( is_array( $default ) && isset( $default[ $key ] ) ) {

			$default_value = $default[ $key ];

		}

		if ( null !== $default_value ) {

			$value = get_theme_mod( $key, $default_value );

		}else {

			$value = get_theme_mod( $key );

		}

		return $value;

	}

endif;


function stunning_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'stunning_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'stunning_customize_partial_blogdescription',
			)
		);
	}
	$default          = stunning_default_theme_options();
	// load all custom settings
	require get_template_directory() . '/ripplethemes/Customizer_settings/customizer_settings.php';
}
add_action( 'customize_register', 'stunning_customize_register' );



/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function stunning_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function stunning_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function stunning_customize_preview_js() {
	wp_enqueue_script( 'stunning-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'stunning_customize_preview_js' );

/**
 * enqueue Script for admin dashboard.
 */

if (!function_exists('stunning_widgets_backend_enqueue')) :
    function stunning_widgets_backend_enqueue($hook)
    {
        

        wp_register_script('stunning-custom-widgets', get_template_directory_uri() . '/assets/js/widget.js', array('jquery'), true);
        
        wp_enqueue_media();
        wp_enqueue_script('stunning-custom-widgets');
    }

    add_action('admin_enqueue_scripts', 'stunning_widgets_backend_enqueue');
endif;