<?php

if (!function_exists('stunning_social_sharing')) :
    function stunning_social_sharing($post_id)
    {
        $stunning_url = get_the_permalink($post_id);
        $stunning_title = get_the_title($post_id);
        $stunning_image = get_the_post_thumbnail_url($post_id);
        
        //sharing url
        $stunning_twitter_sharing_url = esc_url('http://twitter.com/share?text=' . $stunning_title . '&url=' . $stunning_url);
        $stunning_facebook_sharing_url = esc_url('https://www.facebook.com/sharer/sharer.php?u=' . $stunning_url);
        $stunning_pinterest_sharing_url = esc_url('http://pinterest.com/pin/create/button/?url=' . $stunning_url . '&media=' . $stunning_image . '&description=' . $stunning_title);
        $stunning_linkedin_sharing_url = esc_url('http://www.linkedin.com/shareArticle?mini=true&title=' . $stunning_title . '&url=' . $stunning_url);
        
        ?>

<ul class="post-share">
    <li><a target="_blank" href="<?php echo esc_url($stunning_facebook_sharing_url); ?>"><i class="fab fa-facebook-f"></i></a></li>
    <li><a target="_blank" href="<?php echo esc_url($stunning_twitter_sharing_url); ?>"><i class="fab fa-twitter"></i></a></li>
    <li><a target="_blank" href="<?php echo esc_url($stunning_pinterest_sharing_url); ?>"><i class="fab fa-pinterest"></i></a></li>
    <li> <a target="_blank" href="<?php echo esc_url($stunning_linkedin_sharing_url); ?>"><i class="fab fa-linkedin"></i></a></li>
</ul>
<?php
    }
endif;
add_action('stunning_social_sharing', 'stunning_social_sharing', 10);