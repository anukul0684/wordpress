<?php

if (! function_exists('stunning_Socialicons')) :
function stunning_Socialicons(){
?>


<?php

// facebook
if(stunning_get_option('facebook_social')!=''):
    ?>
<li>
    <a target="_blank" rel="noopener noreferrer" href="<?php echo esc_url(stunning_get_option('facebook_social'))  ?>"
        class="social-icon">
        <i class="fab fa-facebook-f"></i>

    </a>
</li>
<?php
endif;

// twitter
if(stunning_get_option('twitter_social')!=''):
    ?>
<li>
    <a target="_blank" rel="noopener noreferrer" href="<?php echo  esc_url(stunning_get_option('twitter_social')) ?>"
        class="social-icon">
        <i class="fab fa-twitter"></i>

    </a>
</li>
<?php
endif;


// google
if(stunning_get_option('google_social')!=''):
    ?>
<li>
    <a target="_blank" rel="noopener noreferrer" href="<?php echo esc_url( stunning_get_option('google_social')) ?>"
        class="social-icon">
        <i class="fab fa-google-plus-g"></i>

    </a>
</li>
<?php
endif;
?>

<?php
}
endif;

add_action('stunning_social_action', 'stunning_Socialicons');