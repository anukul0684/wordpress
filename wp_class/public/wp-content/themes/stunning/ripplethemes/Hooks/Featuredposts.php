<?php

if ( ! function_exists( 'stunning_featuredpost_action' ) ) :
    function stunning_featuredpost_action() { 
       
        
        $args1 = array(
           
        'cat'=>stunning_get_option('stunning-featured-category'),
        
         'order' => 'DESC','posts_per_page'=>stunning_get_option('stunning-featured-no-posts'),
        'orderby'=>'ID'
        );
        $q1 = new WP_query($args1);
        
      
       

        if ($q1->have_posts()) :

                $firstPosts = array();
               
                
                    while ($q1->have_posts()) : 
                        $q1->the_post();

                        if ( stunning_get_option( 'show_featuredposts' ) ) { 
                            // $firstPosts[] = get_the_ID();
                            array_push($GLOBALS['firstPosts'],get_the_ID());
                
                            // featured content
                            get_template_part('ripplethemes/Contents/featured-content');
                        }
                       
                    
                        
                        ?>

<?php
                    endwhile;
                    
        endif;
     
        

    }
endif;


add_action( 'stunning_featuredpost_action', 'stunning_featuredpost_action' );

