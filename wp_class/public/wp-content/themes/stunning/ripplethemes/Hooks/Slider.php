<?php

if (! function_exists('stunning_slider_action')) :
    function stunning_slider_action()
    {
        $args0 = array(
            // 'category_name' => 'beauty',
        'cat'=>stunning_get_option('stunning-slider-category'),
        
         'order' => 'DESC','posts_per_page'=>stunning_get_option('stunning-slider-no-posts'),
        'orderby'=>'ID'
        );
        $q0 = new WP_query($args0);

        ?>
<aside class="section hero">
    <div id="main-slider" class=" main-slider half-slider">
        <?php
              
        if ($q0->have_posts()) :

            // $firstPosts = array();
           
            
                while ($q0->have_posts()) : 
                    $q0->the_post();

                    if ( stunning_get_option( 'stunning-slider-category' ) ) { 

                        get_template_part('ripplethemes/Contents/slider-content');
                   
                    ?>
                        
        <?php

                    }
                   
                endwhile;
            endif;
          
        
        ?>
    </div>

</aside>





<?php


 }
endif;


add_action('stunning_slider_action', 'stunning_slider_action');