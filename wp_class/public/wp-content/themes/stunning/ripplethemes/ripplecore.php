<?php
/* 
loading all core theme setting
* @since 1.0.0
*/


// load custom css and javascript
require get_template_directory() . '/ripplethemes/enqueue.php';

/**
 * 
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/ripplethemes/custom-header.php';
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/ripplethemes/template-tags.php';

/**
 * Template functions for theme.
 */
require get_template_directory() . '/ripplethemes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/ripplethemes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/ripplethemes/jetpack.php';
}


// breadcrumbs setting
require get_template_directory() . '/ripplethemes/breadcrumbs/breadcrumbs.php';


// custom category control
require get_template_directory() . '/ripplethemes/Customizer_settings/customizer-category-control.php';

// Widgets
require get_template_directory() . '/ripplethemes/Widgets/sidebar-author-widget.php';
require get_template_directory() . '/ripplethemes/Widgets/Recent_widgets.php';


// Load all Hooks
require get_template_directory() . '/ripplethemes/Hooks/Otherspost.php';
require get_template_directory() . '/ripplethemes/Hooks/Featuredposts.php';
require get_template_directory() . '/ripplethemes/Hooks/Socialicons.php';
require get_template_directory() . '/ripplethemes/Hooks/breadcrumb.php';
require get_template_directory() . '/ripplethemes/Hooks/Slider.php';
require get_template_directory() . '/ripplethemes/Hooks/Related_post.php';
require get_template_directory() . '/ripplethemes/Hooks/Social_share.php';
require get_template_directory() . '/ripplethemes/TGM-Plugin-Activation/tgm.php';













