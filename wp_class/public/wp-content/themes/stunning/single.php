<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package stunning
 */
get_header();

?>
<div class="custom-breadcrumb">
    <div class="container">
        <div class="breadcrumb-title is-start">
            <?php
                echo esc_html( get_the_category()[0]->cat_name);
                ?>

        </div>
        <?php 
            if(stunning_get_option('stunning_show_breadcrumbs')){
                do_action( 'stunning_breadcrumb_options' ) ;


            }
         
         ?>



    </div>

</div>
<div class="container">
    <div class="row">
    <div id="primary" class="content-area rpl-lg-9">
    <main id="main" class="site-main">

        <?php
		while ( have_posts() ) :
            the_post();
            ?>


        <?php get_template_part('ripplethemes/Contents/single-content'); ?>
        <?php
        
        if(stunning_get_option('stunning_post_next_post_link')){
            the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'stunning' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'stunning' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

        }

        
        if(stunning_get_option('stunning_show_related')) :
            do_action( 'stunning_related_post' );
         endif; 
      
			

            ?>


        </main>
        <?php
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

        endwhile; 
        ?>
    </div>
    <div class="rpl-lg-3" id="sidebar-secondary">
        <?php
           get_sidebar() ?>
    </div>


</div>







</div>

<?php

    get_footer();
    ?>