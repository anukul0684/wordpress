<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stunning
 */


get_header() ?>

<div class="custom-breadcrumb">
    <div class="container">
        <div class="breadcrumb-title is-start">
            <?php
                the_archive_title('<h1 class="title">', '</h1>');
                the_archive_description('<div class="archive-description">', '</div>');
                ?>

        </div>
        <?php 
            if(stunning_get_option('stunning_show_breadcrumbs')){
                do_action( 'stunning_breadcrumb_options' ) ;


            }
         
         ?>



    </div>

</div>
<div id="content" class="site-content global-layout-no-sidebar">

    <div class="container">
        <div class="row">
            <div id="primary" class="content-area rpl-lg-9">
                <main id="main" class="site-main">

                    <div class="grid-view">
                        <div class="row">
                            <?php
                        if (have_posts()):
                            while (have_posts()):
                                the_post();
                        ?>


                            <?php get_template_part('ripplethemes/Contents/Otherspostfirst'); ?>


                            <?php
                        endwhile;
                        else :

                            get_template_part( 'template-parts/post/content', 'none' );
                        endif;
                        ?>


                        </div>
                        <nav class="navigation pagination">
                            <div class="nav-links is-center">
                                <?php
                           
                                the_posts_pagination();
                                ?>

                            </div>
                            <!-- .nav-links -->
                        </nav>
                    </div>

                </main>
            </div>

            <div class="rpl-lg-3" id="sidebar-secondary">

                <?php get_sidebar() ?>

            </div>


        </div>

    </div>
</div>

<?php get_footer() ?>