<?php
/**
 * Search Form 
 *
 * @package stunning
 */

?>

    
<div class="searchform" role="search">
        <form role="search" method="get" action=<?php echo esc_url(home_url( '/' )); ?>  >
        <label>
            <span class="screen-reader-text"> <?php  esc_html_e('Search for:', 'stunning') ?> </span>
            <input type="search" class="search-field" placeholder= "<?php  esc_attr_e('Search...','stunning')?>" value="<?php the_search_query(); ?>" name="s">
        </label>
        <input type="submit" class="search-submit" value="Search">
    </form>			
</div>
