=== stunning ===

Contributors: ripplethemes
Requires at least: 4.5
Tested up to: 5.4.2
Requires PHP: 5.6
Stable tag: 1.0.9
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready,two columns,three columns


== Description ==

Stunning is a Free WordPress Theme perfect for online bloggers, writers, travelers & photographers. This theme has an awesome layout for the home page and single page.This WordPress is clean and responsive theme with Grid and list Post styles which gives the website a clean and sturdy look.
Theme Demo here https://ripplethemes.com/downloads/stunning-wordpress-theme/.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

stunning includes support for WooCommerce and for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0.9 - Aug 15 2020 =
* Design issue fixed

= 1.0.8 - Aug 8 2020 =
* Design issue fixed

= 1.0.7 - Aug 6 2020 =
* article transparent background feature added

= 1.0.6 - Aug 5 2020 =
* Plugin notification added

= 1.0.5 - Aug 4 2020 =
* Design issue fixed

= 1.0.4 - Aug 3 2020 =
* funtional issue fixed

= 1.0.3 - Aug 1 2020 =
* funtional issue fixed

= 1.0.2 - July 28 2020 =
* Added recent post and footer logo option
* Design issue fixed


= 1.0.1 - July 8 2020 =
* Broken line issue solved in style.css

= 1.0.0 - July 8 2020 =
* Initial release

== Credits ==

Stunning is free WordPress theme by, Ripplethemes.
Stunning is distributed under the terms of the GNU GPL v2 or later.

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
* BreadcrumbTrail, Copyright (c) 2008 - 2015, Justin Tadlock http://themehybrid.com/plugins/breadcrumb-trail
* Slick Slider https://github.com/kenwheeler/slick/blob/master/LICENSE, Copyright (c) 2017 Ken Wheeler, [The MIT License (MIT)]
* Font Awesome Free 5.13.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

== Screenshots ==

Image for theme screenshot, Copyright Burst
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/woman-beach-SS2GVMJTWH

Image for theme screenshot, Copyright Burst
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/woman-shop-E1KVAKFMUK

Image for theme screenshot, Copyright Burst
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/woman-sipping-2ACDGXWMJJ


Image for theme screenshot, Copyright Elliott Chau
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/walking-city-IZJKSG1FLK

Image for theme screenshot, Copyright PxHere
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1209975