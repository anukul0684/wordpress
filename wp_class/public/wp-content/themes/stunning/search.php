<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package stunning
 */

get_header() ?>

<div class="custom-breadcrumb">
    <div class="container">
        <div class="breadcrumb-title is-start">
            <?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for:  %s', 'stunning' ), '<span>'." " . get_search_query() . '</span>' );
					?>
        </div>

        <?php 
            if(stunning_get_option('stunning_show_breadcrumbs')){
                do_action( 'stunning_breadcrumb_options' ) ;


            }
         
         ?>
    </div>

</div>
<div id="content" class="site-content global-layout-no-sidebar">

    <div class="container">
        <div class="row">
            <div id="primary" class="content-area rpl-lg-9">
                <main id="main" class="site-main">

                    <div class="grid-view">
                        <div class="row">
                            <?php 
                        if(have_posts()):
                            while(have_posts()):
                                the_post(  );
                        ?>


                            <?php
                             get_template_part('ripplethemes/Contents/Otherspostfirst');
                           
                        endwhile;

                    else:
                         get_template_part('ripplethemes/Contents/content','none');
                         
                        endif;
                        ?>


                        </div>
                        <nav class="navigation pagination">
                            <div class="nav-links is-center">
                                <?php  
                                // global $wp_query; 
                                the_posts_pagination( ) ;?>

                            </div>
                            <!-- .nav-links -->
                        </nav>
                    </div>

                </main>
            </div>
            <div class="rpl-lg-3" id="sidebar-secondary">

                <?php get_sidebar() ?>

            </div>
        </div>

    </div>
</div>

<?php get_footer() ?>