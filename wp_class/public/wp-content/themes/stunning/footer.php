<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stunning
 */

?>



<footer class="site-footer">
    <div class="footer-in">
        <div class="container">
            <?php

               
                if(stunning_get_option('show_logo_in_footer')=='1'){
                     the_custom_logo(); 
         
                }
                
                ?>
            <div class="footer-menu">
                <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'footer-menu',
                                    'menu_id'        => 'footer-menu',
                                )
                            );
                        ?>
            </div>
            <ul class="ft-social">

                <?php do_action('stunning_social_action')?>

            </ul>
        </div>

    </div>
    <div class="site-info">
        <div class="container">
            <div class="siteinfo-text">

                <?php 
               echo wp_kses_post( stunning_get_option('stunning_footer_copyright') ) ;
                ?>



                <?php
                echo wp_kses_post(stunning_get_option('stunning_footer_poweredby')) ;

                ?>


            </div>
        </div>
    </div>

</footer>


<?php wp_footer() ?>

</div>

</body>

</html>