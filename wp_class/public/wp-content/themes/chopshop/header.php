
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?=get_stylesheet_uri(); ?>" />
	<meta name='robots' content='noindex,follow' />
    <?php wp_head(); ?>
</head>
<body>
    <div class="container">

        <header class="main">

            <img src="http://chopshop.ayntechnologies.com/wp-content/uploads/2020/11/header.jpg" alt="The Chopshop" />

            <span class="site_title">Chopshop</span>

        </header>