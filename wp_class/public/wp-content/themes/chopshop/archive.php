<?php

/**
 * archive/category template for ChopShop
 */

 get_header();

?> <div id="content">
        <div id="primary">

            <article>
                <!-- display category or archive selected -->
                <h1 class="archive_title"><?=the_archive_title()?></h1>

                <!-- run a loop through all that belongs to the selected category or archive -->
                <?php while( have_posts() ) : ?>

                    <?php the_post(); ?>
                    
                    <!-- a list view hence added permalink for visiting the post details-->
                    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                    
                    <!-- a brief exhibit of the post -->
                    <div class="content">
                        <?php the_excerpt(); ?>
                    </div>
                <?php endwhile ?>
                
            </article>

        </div><!-- /primary -->

        <?php get_sidebar(); ?>

    </div><!-- /content -->

<?php 

    get_footer();

?>