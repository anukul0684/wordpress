<?php

/**
 * Search template for ChopShop
 */

 get_header();

?>  <div id="content">
		<div id="primary" class="single">

			<article>
                <!-- display the content searched in search form here -->
                <h1 class="archive_title"><?=get_search_query()?></h1>

                <!-- run loop for all the posts fetched for the searched content -->
                <?php while( have_posts() ) : ?>

                    <?php the_post(); ?>
                    
                    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                    
                    <div class="content">
                        <?php the_excerpt(); ?>
                    </div>

                <?php endwhile ?>
				
			</article>			
			
		</div><!-- /primary -->
		
		<?php get_sidebar(); ?>

	</div><!-- /content -->

<?php 

    get_footer();

?>