<?php
/**
 * 404 Index template for Blog
 */

    get_header(); // adding header to the page
    
?>

<div id="content" class="col-lg-12">

    <div id="primary" class="col-md-10">
    
        <article>
            <!-- add content to display on page not found -->
            
            <h1 class="archive_title">404 - Not Found </h1>
            
            <p> page does not exist</p>
                
        </article><!-- /article -->

    </div><!-- /primary -->

    <?php get_sidebar(); ?> <!-- adding sidebar -->

</div><!-- /content -->

<?php get_footer();?> <!-- adding footer -->