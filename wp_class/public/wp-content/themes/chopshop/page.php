<?php

/**
 * Page template for ChopShop
 */

 get_header();

?> <div id="content">
        <div id="primary">

            <article>
                <!-- show the page name called -->
                <h3><?php the_title(); ?></h3>

                <!-- show the page content -->
                <?php the_content(); ?>

            </article>

        </div><!-- /primary -->

        <?php get_sidebar(); ?>

    </div><!-- /content -->

<?php 

    get_footer();

?>