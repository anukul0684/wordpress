        <footer class="main">
            <!-- added menu list default -->
            <div class="menu">
                <ul>
                    <?php wp_nav_menu() ?> 
                </ul>
            </div>

            <p class="copyright">Copyright &copy; 2015 by Chopshop</p>

        </footer>
    </div>

<?php wp_footer(['menu' => 'footer']); ?>    

</body>
</html>