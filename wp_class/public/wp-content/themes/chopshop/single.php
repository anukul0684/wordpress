<?php

/**
 * single template for ChopShop
 */

 get_header();

?> <div id="content">
        <div id="primary">

            <article>
                <!-- show the post title -->
                <h3><?php the_title(); ?></h3>    

                <!-- show the tags it belongs to  -->
                <p> 
                    <?=get_the_tag_list(sprintf( '<p>%s: ', __( 'Tags', 'textdomain' ) ), ', ', '</p>' ); ?>                        
                </p>

                <!-- show the post details -->
                <?php the_content(); ?>

                <!-- Display comment template from installed plugin wpDisCuz -->
                <?php comments_template();?>

            </article>

        </div><!-- /primary -->

        <!-- display only the main menu in the sidebar -->
        <?php get_sidebar('post'); ?>

    </div><!-- /content -->

<?php 

    get_footer();

?>