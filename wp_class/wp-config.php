<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_class' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k-!=Ku+f}J9>qb3+.!6?R@HIjN3QLr^--(1|nc3$NdG-<(p,p?pVAC+ULx=q,!&I');
define('SECURE_AUTH_KEY',  '4GF^||A,!zlslSgr5-r&$dZ4Vd]QG8EPQ+},D}igd=UO+;CuK<}-[+70gcFOvN!~');
define('LOGGED_IN_KEY',    'aXO;J)t&SZ;^;[FgXKHBXh.0dbYF8#tjokpTS1Dy1?BVW||Va~>!z`%&|$jkXKG_');
define('NONCE_KEY',        'Hq}c+|0*jh;opmPD>E8iYMGj79!4H-tR$uVo#^/h(]5UcZ!~{EQR{.^M?s@cBE`0');
define('AUTH_SALT',        '-p^d2CoUnb}9VvgZ?1M+9G^y/rp#{_;Z0jY Cs+lpeG7I3>1)_u<rz>aNc+wik|0');
define('SECURE_AUTH_SALT', 'D[/9nth)h|v/l%`fF[n^QADp}iQ8AEBab3b~.n%%jk;A*,f{aauqq+E)lAXf}3c?');
define('LOGGED_IN_SALT',   'bO-aJs-LhbR7F-/vdfrE^YU_2*~:gfzb-KrP7P.0VOa3-y-&w36O4<4<#o2@f)~G');
define('NONCE_SALT',       ',a,4*.Bf~Cj@)`c pIV= u)pE[DNm-sc-9^gdzC3lt>M9!E|5+sf(n`&n!Vh,$Jf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

define('WP_DEBUG_DISPLAY',true);

@ini_set('display_errors',1);

@ini_set('error_reporting',E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
