<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- <title>Blogit!</title> -->
	
	<title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>
	
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	
	<link type="text/css" rel="stylesheet" href="<?=get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
</head>
<body>
	<div class="container">

		<header class="row main" style="padding-top: 4%; background: #ccc; margin: 0;">

			<div class="col-md-2 text-center">
				<h1>Blogit!</h1>
			</div>

			<div class="col-md-10 text-center">
				<nav>
					<ul class="list-inline h2">
						<?php wp_nav_menu() ?>
					</ul>
				</nav>
			</div>

		</header>