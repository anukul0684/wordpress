
<div id="secondary" class="col-md-2">
								
    <div class="categories">
        <h3>Categories</h3>
        <ul class="menu">
            <?php wp_list_categories(['title_li' => '', 'depth' => 1]); ?>
        </ul>
    </div>
    
    <?php if(is_single()) :?>
        <p><small><?=get_the_tag_list(); ?></small></p>
    <?php endif; ?>
    
    <div class="archives">
        <h3>Archive</h3>
        <ul class="menu">
            <?php wp_get_archives() ?>
        </ul>
    </div>
    
</div><!-- /secondary -->