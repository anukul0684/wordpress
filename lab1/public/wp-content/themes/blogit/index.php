<?php
/**
 * Index template for Blog
 */

	get_header();
?>

		<div id="content" class="col-lg-12">

			<div id="primary" class="col-md-10">
			
				<article>

				<?php if(is_category()) : ?>
					<h1 class="archive_title"><?=the_archive_title()?></h1>
				<?php endif; ?>

				<?php while( have_posts() ) : ?>

					<?php the_post(); ?>

					<?php if(is_page() || is_single()) : ?>
						<h3><?php the_title(); ?></h3>
					<?php else: ?>
						<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
					<?php endif ?>

					<div class="content">
						<?php if(is_page() || is_single()) : ?>
							<?php the_content(); ?>
						<?php else: ?>
							<?php the_excerpt(); ?>
						<?php endif ?>
					</div>
				<?php endwhile ?>
						
				</article><!-- /article -->

			</div><!-- /primary -->

			<?php if(is_single() || is_page('Blog')) : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>

			<?php if(is_page()) : ?>
				<?php get_sidebar('page'); ?>
			<?php endif; ?>

		</div><!-- /content -->

		<?php get_footer();?>