<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_lab1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T+[VW01sweu2GN;xN4MnkmhCn^S%w%cN+W9BSK[lSKW=XFcZ2^,X[*{@hF>Gv6m*');
define('SECURE_AUTH_KEY',  '8[>l(PB,>{]MPv1e~W4rSe U>reFrgbT-[516$ COd{M>ODF]U,^u2/u25u=5&!q');
define('LOGGED_IN_KEY',    'MNfJ8O_g^K^-tc(#ZAj&V/m9F;Q(Z#-p-OVO0&.)3g7EB|W/|I!A&yD9%5_K$BOu');
define('NONCE_KEY',        '&8@^uu0=!*(<tsH^,|8;?!pnDTR|%IwbsTC!c?#a9xN2Jbzg7+ls;Xp@`t2,-NiE');
define('AUTH_SALT',        'OKL_QI-d%d;k=&g-jC*cs+S}gOymDnV!6m-`h.I?M?)+~yXxK|!ZB6AfQye,=WtL');
define('SECURE_AUTH_SALT', 'prm<]7P*BX) 1T7/LX~JP!v6T;`=]`@+g*TWD em3g!`(d[c2eGUtA>f*ku&<5h!');
define('LOGGED_IN_SALT',   'Y%C~#1*T%VvUlu+LY~)WwH<b*rubS9hH##l$9]uF8gnCj#-IKpQXM24h~@ni0Mm,');
define('NONCE_SALT',       '-EE}6v!9n}~2p7>Nsk&R(Gw6(<$b0=%Yei<R_LN4TE?k:OKA3Cf?&w$g4`oxZT;%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL);


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
