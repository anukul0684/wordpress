<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'profile_anu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';-5yzt5(-@tvoO-nv .[h3j9,}qj+%;-o3p9%m`l.(AaG|l):X?,_MD!h0rM?KDK');
define('SECURE_AUTH_KEY',  'Pm-tI+-pyx&_t(FOglX1DvK`^, yb~OB$U{y@o~/Hlv?oI)O/6~mz592G96oDC&e');
define('LOGGED_IN_KEY',    '3I^V,MF^pZ4|-5QB^5z89?g,+SaI4pJS;ziBfuLOO9uV%2]9ZvP:@k%pDr[$Rk%|');
define('NONCE_KEY',        '1[8vu7ffPg97eR-IN=8^K+>VM5.<!`}5*Gf<V%r|5e}0+>v)+>dIyuEkfL.OG7(v');
define('AUTH_SALT',        '/nx|.}mm2tv`q|EO|v_s~TG::|IHCYNCh>cr~32Qc&-{cnQu5^V+^oA+Y`zxF9w-');
define('SECURE_AUTH_SALT', 'k22.wmB3UbI3_D1|Fu%Y$w&s+q<|4:r%h(s8~={#|a_KD?Dg#X+IOlrv#xg`Q-~z');
define('LOGGED_IN_SALT',   '|-0xf~E{K,*7c-EtvM1LI&Lmn+]k1J$b*D_S]d$wqG+Kx_zFmbf#*_n1@*-EN-xk');
define('NONCE_SALT',       ':3e(E <?Op-1nA[t_kcqYEc><fF)T4Hbf:+a(e?p}dkxKri%|dcM/[v$R#qc =yq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
