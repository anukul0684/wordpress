</div>
    <!-- Footer -->
    <footer class="py-5 bg-secondary">
        <div class="container">
            <?php wp_nav_menu(['menu'=>'footer-menu', 'menu_class'=>'lead']); ?>            
            <h4><?php wp_nav_menu(['menu'=>'contact-nav']); ?></h4>
            <p class="m-0 text-center text-white">Copyright &copy; Portfolio 2020</p>
        </div>
        <!-- /.container -->
    </footer>   
    <?php wp_footer(); ?>
</body>
</html>