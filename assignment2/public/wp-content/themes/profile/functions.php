<?php

/** 
 * Functions file for including functions/filters/menus/featuredimages in current theme
 */

if(!function_exists('profile_scripts'))
{
    function profile_scripts()
    {

        wp_enqueue_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css',
                            [],'4.5.2',false);

        wp_enqueue_style('profile',get_stylesheet_uri(),['bootstrap'], '1.0', false);

        wp_enqueue_style('googleapis','https://fonts.googleapis.com/css?family=Nunito');
        wp_enqueue_style('dns-prefetch','//fonts.gstatic.com');

        wp_enqueue_script('bootstrap-bundle',get_template_directory_uri() . '/js/bootstrap.bundle.min.js',
                           ['jQuery'],'1.0',true);
        wp_enqueue_script('jQuery',get_template_directory_uri() . '/js/jquery.min.js',
                           [],'1.0',true);        
    }

    add_action('wp_enqueue_scripts','profile_scripts');
}

if(!function_exists('profile_custom_post_type')) {
    function profile_custom_post_type()
    {
        // labels (singular) - array
        // public - boolean
        // capability_type - post or page
        // has_archive - boolean
        // heirarchical - boolean
        // taxonomies - array eg.: ['category','tag']
        // supports - array
        $params = array(
            'labels' => [
                'name' => 'Projects',
                'singular_name' => 'Project'
            ],
            'public' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => true,
            'taxonomies' => ['category'],
            'supports' => [
                'title',
                'editor',
                'excerpt',
                'custom-fields',
                'revisions',
                'page-attributes',
                'post-thumbnails'
            ]
        );
        register_post_type('Project',$params);
    }
    add_action('init','profile_custom_post_type');

    //flush_rewrite_rules(true);
}


if(!function_exists('the_project_title')) {
    function the_project_title()
    {
        global $post;
        $name = get_post_meta($post->ID,'project_name',true);
        echo esc_html($name);
    }

}

if(!function_exists('the_project_img')) {
    function the_project_img()
    {
        global $post;        
        $id = get_post_meta($post->ID, 'project_image',true);
        echo wp_get_attachment_image($id,'medium');
    }

}

if(!function_exists('the_project_course')) {
    function the_project_course()
    {
        global $post;
        $course = get_post_meta($post->ID,'project_course',true);
        echo esc_html($course);
    }

}

if(!function_exists('the_project_desc')) {
    function the_project_desc()
    {
        global $post;
        $desc = get_post_meta($post->ID,'description',true);
        echo esc_html($desc);
    }

}

if(!function_exists('the_project_technologies')) {
    function the_project_technologies()
    {
        global $post;
        $technologies = get_post_meta($post->ID,'technologies_used',true);
        echo esc_html($technologies);
    }

}

if(!function_exists('the_project_link')) {
    function the_project_link()
    {
        global $post;
        $link = get_post_meta($post->ID,'project_link',true);
        if($link=='') { 
            $link = 'Not Hosted Live'; 
            echo esc_html($link);
            return;
        }        
        esc_html($link);
        $link = "<a class=\"text-light\" href=\"{$link}\" title=\"Click here to visit the website\"
                target=\"_blank\">" . $link . "</a>";
        echo $link;
    }

}

if(!function_exists('the_project_carousel')) {
    function the_project_carousel()
    {
        global $post;
        $carousel = get_post_meta($post->ID,'project_carousel_code',true);
        echo esc_html($carousel);
    }

}

// ShortCode for button on single page
if(!function_exists('i_need_to_go_back')) {
    function i_need_to_go_back($atts,$content='')
    {
        $link = $atts['link'] ?? '#';
        $out = "<div style=\"padding:20px 0; font-size:1.6rem;\">
                    <p class=\"text-center\">
                        <a class=\"btn btn-lg btn-success\" href=\"{$link}\">
                            {$content}

                        </a>
                    </p>
                </div>";
        return $out;
    }

    add_shortcode('goBack','i_need_to_go_back');
}

if(!function_exists('i_dont_want_to_do_this')) {
    function i_dont_want_to_do_this($atts,$content='')
    {
        $link = $atts['link'] ?? '#';
        $out = "<a style=\"padding:10px 15px; font-size:1.1rem; border-radius: 0;\" 
                    class=\"btn btn-danger\" 
                    href=\"{$link}\">{$content}</a>";
        return $out;
    }

    add_shortcode('rejectCancel','i_dont_want_to_do_this');
}