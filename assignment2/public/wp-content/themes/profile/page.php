<?php

/** 
 * Page template for Page detail view
 */

    get_header();

?>  <div class="row">
        <article class="text-light col-md-8">            
            <?php while(have_posts()) : the_post(); ?>                                  
                <h1 class="mx-4"><?php the_title(); ?></h1>                     
                               
                <div class="mx-4"><?php the_content(); ?></div>
            <?php endwhile; ?>
        </article>   
        <div class="col-md-4"> 
            <?php get_sidebar(); ?>
        </div>
    </div>
    </section>
    <?php get_footer(); ?>