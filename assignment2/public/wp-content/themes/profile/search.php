<?php

/** 
 * Search template for search list view
 */

    get_header();

?>   
        <article class="text-light">   
            <h1> You have searched for "<?php the_search_query(); ?>" </h1>
            <h2> we have: <?=count($posts)?> result(s) </h3>
            <?php if($posts): ?>         
                <?php while(have_posts()) : the_post(); ?>
                    <h3>
                        <a href="<?php the_permalink(); ?>" class="text-white" title="click here for details">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <p><small>Posted on <?php the_date() ?></small></p>
                    <?php the_excerpt(); ?>
                <?php endwhile; ?>
            <?php else: ?>
                <h3>Try again.</h3>
                <div class="search_box">
                    <?php get_search_form(); ?>
                </div>
            <?php endif; ?>
        </article>    
    </section>
<?php get_footer(); ?>