<?php

/** 
 * Single template for Detail view
 */

    get_header();

?>  
    <div class="row">
        <article class="text-light col-md-8">            
            <?php while(have_posts()) : the_post(); ?>
                <h1 class="mx-4"><?php the_title(); ?></h1>                
                <div class="mx-4 px-4 mt-4"><?php the_project_img(); ?></p>
                <p><strong><em>Project Name</em></strong>: <?php the_project_title(); ?></p>
                <p><strong><em>Technologies used</em></strong>: <?php the_project_technologies(); ?></p>
                <p><strong><em>Project Course</em></strong>: <?php the_project_course(); ?></p>
                <p><strong><em>Project Description</em></strong>: <?php the_project_desc(); ?></p>
                <p><strong><em>Project Link</em></strong>: <?php the_project_link(); ?></p>                
            <?php endwhile; ?>
            <?php echo do_shortcode("[goBack link=\"/project\"] Back to Projects [/goBack]"); ?>
                <!-- //[goBack link="/project"]Back[/goBack] -->
        </article>   
        <div class="col-md-4">
            <?php get_sidebar(); ?>
        </div>
    </div> 
    </section>
    <?php get_footer(); ?>