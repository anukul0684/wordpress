<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
        
    <?php wp_head(); ?>
</head>
<body class="mt-3 pt-4">
    <div id="app" >
        <nav id="main-nav" class="navbar navbar-expand-md navbar-light bg-danger shadow-sm fixed-top py-3">
            <div class="container">
                <a class="navbar-brand lead text-white" href="<?php the_permalink(6)?>">
                    <strong><?=strtoupper('Profile'); ?></strong>
                </a>
                <button class="navbar-toggler" type="button" 
                    data-toggle="collapse" data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" aria-expanded="false" 
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">                   

                    <!-- Right Side Of Navbar -->
                    <!-- <ul class="navbar-nav ml-auto lead text-dark"> -->
                    <div style="width: 100%; text-align: right; padding: 0; margin: 0;">
                        <?php wp_nav_menu(['menu'=>'main-nav', 'menu_class'=>'navbar-nav ml-auto lead main-menu']); ?>  
                    </div>            
                    <!-- </ul> -->
                </div>
            </div>
        </nav>
        <section class="container bg-secondary mb-1 pt-1 pb-4 shadow-sm opacity-5">
        <?php if(is_front_page()) : ?>
            <!-- Jumbotron Header -->
            <div class="jumbotron my-4 row mx-4" 
                style="background-image: url('<?php bloginfo('template_directory'); ?>/images/background1.jpg ');">
                <div id="left-div" class="col-sm-7 text-center">
                    <h1 class="display-3">Hi! I am Anu...</h1>
                </div>
                <div id="right-div" class="col-sm-5">
                    <p id="intro1" class="lead h3">
                        A Full Stack Web Developer,              
                    </p>
                    <p id="intro2" class="lead h3">
                        a writer,               
                    </p>
                    <p id="intro3" class="lead h3">
                        and a soulful cook...               
                    </p>
                    <p id="show-more" class="lead h2 bg-gradient-secondary">
                        Please <a href="<?php the_permalink(16)?>" 
                        class="btn btn-danger btn-lg"> Click </a> here to know more!
                    </p>
                </div>
            </div>              
        <?php elseif(is_archive() || is_page() || is_home() || is_single()) : ?>   
            <?php $page_id = $post->ID;
                $post_id = $wp_query->get_queried_object_id();
                //var_dump($post_id);
                if($page_id == 93 && $post_id != 0) : ?>                        
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="129"]'); ?></div>
                <?php elseif($page_id == 99 && $post_id!= 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="132"]'); ?></div>
                <?php elseif($page_id == 101 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="135"]'); ?></div>
                <?php elseif($page_id == 118 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="156"]'); ?></div>
                <?php elseif($page_id == 103 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="138"]'); ?></div>
                <?php elseif($page_id == 109 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="144"]'); ?></div>
                <?php elseif($page_id == 115 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="150"]'); ?></div>
                <?php elseif($page_id == 112 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="147"]'); ?></div>
                <?php elseif($page_id == 106 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="141"]'); ?></div>
                <?php elseif($page_id == 227 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="21"]'); ?></div>
                <?php elseif($page_id == 242 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="40"]'); ?></div>
                <?php elseif($page_id == 245 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="248"]'); ?></div>
                <?php elseif($page_id == 258 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="264"]'); ?></div>
                <?php elseif($page_id == 277 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="280"]'); ?></div>
                <?php elseif($page_id == 290 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="303"]'); ?></div>
                <?php elseif($page_id == 293 && $post_id != 0): ?>
                    <div id="carousel" class="col my-4 rounded d-flex justify-content-center"><?php echo do_shortcode('[soliloquy id="296"]'); ?></div>
                <?php elseif(is_archive() || is_page() || is_home() || is_single()): ?>
                    <div class="row">
                        <div id="page_header" class="col mx-4 my-4 rounded">
                            <img src="<?=get_template_directory_uri(); ?>/images/header_all1.jpg" 
                            alt="Page Image" class="rounded"/>
                        </div>
                    </div><!-- /row -->
                <?php endif; ?>
            <?php endif; ?>