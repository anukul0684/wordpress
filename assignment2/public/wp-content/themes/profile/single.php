<?php

/** 
 * Single template for Detail view
 */

    get_header();

?>  
    <div class="row">
        <article class="text-light col-md-8">            
            <?php while(have_posts()) : the_post(); ?>
                <h1 class="mx-4"><?php the_title(); ?></h1>                        
                
                <p class="mx-4"><small>Posted on <?php the_date(); ?> By <?php the_author(); ?></small></p>
                <div class="blockquote mx-4">
                    <?php the_content(); ?>
                </div>
                <?php echo do_shortcode("[goBack link=\"/blog\"] Back to Blog Posts [/goBack]"); ?>
                <div class="mx-4">
                    <?php comments_template(); ?>
                </div>                
            <?php endwhile; ?>
        </article>   
        <div class="col-md-4">
            <?php get_sidebar(); ?>
        </div>
    </div> 
    </section>
    <?php get_footer(); ?>