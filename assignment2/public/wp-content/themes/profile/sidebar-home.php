<!-- section to show the home page view -->
<div id="services">
    <div class="container">        
        <div class="row text-center">
            <?php $featuredpages = get_pages(['meta_key' => 'featured', 'meta_value' => '1']); ?>
            <?php $contactpage = get_pages(['include' => ['12']]); ?>
            <?php //var_dump($featuredpages); 
                foreach($featuredpages as $post) : ?>
                <div class="col-lg-4 col-md-6 mb-4 mt-4">
                    <div class="card h-100">    
                        <a href="<?=get_the_permalink($post->ID)?>" title="Click here to view details">                    
                            <!-- class="card-img-top" -->
                            <?=get_the_post_thumbnail($post->ID); ?>
                        </a>
                       
                        <div class="card-footer">
                            <a href="<?=get_the_permalink($post->ID);?>" class="btn btn-danger btn-block"><?=get_the_title($post->ID);?></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?> 
            <?php //var_dump($featuredpages); 
                foreach($contactpage as $post) : ?>
                <div class="col-lg-4 col-md-6 mb-4 mt-4">
                    <div class="card h-100">    
                        <a href="<?=get_the_permalink($post->ID)?>" title="Click here to view details">                    
                            <!-- class="card-img-top" -->
                            <?=get_the_post_thumbnail($post->ID); ?>
                        </a>
                       
                        <div class="card-footer">
                            <a href="<?=get_the_permalink($post->ID);?>" class="btn btn-danger btn-block"><?=get_the_title($post->ID);?></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>                                      
        </div>            
    </div>
</div>
       