<?php

/** 
 * Index template for list view
 */

    get_header();

?>  <div class="row"> 
        <article class="text-light col-md-8">
            <h1 class="mx-4">Recent posts</h1>
            <?php while(have_posts()) : the_post(); ?>
                <h2 class="mx-4">
                    <a href="<?php the_permalink() ?>" title="click to see details" class="text-white">
                        <?php the_title(); ?>
                    </a>    
                </h2>
                <p class="mx-4"><small>Posted on <?php the_date(); ?> By <?php the_author(); ?></small></p>
                <div class="mx-4"><?php the_excerpt(); ?></div>
            <?php endwhile; ?>
        </article> 
        <div class="col-md-4">
            <?php get_sidebar(); ?>
        </div>   
    </div>
    </section>
<?php get_footer(); ?>