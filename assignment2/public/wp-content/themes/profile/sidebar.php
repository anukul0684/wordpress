<!-- section to show the home page view -->
<div id="services-block">
    <div class="container">        
        <div class="row">
            <?php $featuredpages = get_pages(['meta_key' => 'featured', 'meta_value' => '1']); ?>
            <?php $contactpage = get_pages(['include' => ['12']]); ?>
            <ul class="ml-3 pl-2">
            <?php foreach($featuredpages as $post) : ?>
                <li>         
                    <h4 class="bg-light mb-0 mt-3 rounded-top">
                        <a href="<?=get_the_permalink($post->ID);?>" class="text-dark">
                            <?=get_the_title($post->ID);?>
                        </a>
                    </h4>            
                    <a href="<?=get_the_permalink($post->ID)?>" 
                        title="Click here to view details">                        
                        <?=get_the_post_thumbnail($post->ID); ?>
                    </a>                                        
                </li>
            <?php endforeach; ?>   
            <?php foreach($contactpage as $post) : ?>
                <li>         
                    <h4 class="bg-light mb-0 mt-3 rounded-top">
                        <a href="<?=get_the_permalink($post->ID);?>" class="text-dark">
                            <?=get_the_title($post->ID);?>
                        </a>
                    </h4>            
                    <a href="<?=get_the_permalink($post->ID)?>" 
                        title="Click here to view details">                        
                        <?=get_the_post_thumbnail($post->ID); ?>
                    </a>                                        
                </li>
            <?php endforeach; ?> 
            </ul>                                 
        </div>            
    </div>
</div>
       