<?php

/** 
 * Archive template for date related post list view
 */

    get_header();

?>  <div class="row">
        <article class="text-light col-md-8">      
            <h1 class="mx-4"> <?=get_the_archive_title(); ?> </h1>     
            <?php while(have_posts()) : the_post(); ?>
                   
                <h3 class="mx-4">
                    <a href="<?php the_permalink(); ?>" class="text-white" title="click here for details">
                        <?php the_title(); ?>
                    </a>
                </h3>                    
                               
                <div class="mx-4 px-4"><?php the_excerpt(); ?></div>
            <?php endwhile; ?>
        </article>    
        <div class="col-md-4"> 
            <?php get_sidebar(); ?>
        </div>
    </div>
    </section>
<?php get_footer(); ?>