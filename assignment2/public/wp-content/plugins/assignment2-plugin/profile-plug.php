<?php
/**
 * Plugin Name: Profile Plugin
 * Description: functions common for all themes in this wordpress folder
 * Author: Anu Kulshrestha
 * Author URI: http://profile.ayntechnologies.com
 * Plugin URI: http://profile.ayntechnologies.com/wordpress/assignment2/wp-content/plugins/assignment1-plugin/profile-plug
 * Description: Settings common to all Profile themes... 
 * Version: 1.0
 */

if(!function_exists('profile_config_theme')) {

    /**
     * 
     */
    function profile_config_theme()
    {
        register_nav_menus();

        add_theme_support('post-thumbnails');
    }

    add_action('init', 'profile_config_theme');
}

if(!function_exists('profile_echo_note')) {
    /**
     * 
     */
    function profile_echo_note()
    {
        echo "\n" . "<!--This Theme uses Profile Theme Settings -->";
    }

    add_action('wp_head', 'profile_echo_note', 10);
}

// if(!function_exists('profile_title')) {
//     function profile_title($title)
//     {
//         return strtoupper($title);
//     }

//     add_filter('the_title','profile_title');
// }

// reference : https://developer.wordpress.org/reference/functions/comment_form/
//  reorganizing comments form
if(!function_exists('mo_comment_fields_custom_order')) {

    function mo_comment_fields_custom_order( $fields ) {
        $comment_field = $fields['comment'];
        $author_field = $fields['author'];
        $email_field = $fields['email'];
        $url_field = $fields['url'];
        $cookies_field = $fields['cookies'];
        unset( $fields['comment'] );
        unset( $fields['author'] );
        unset( $fields['email'] );
        unset( $fields['url'] );
        unset( $fields['cookies'] );
        unset( $fields['wp-comment-cookies-consent']);
        // the order of fields is the order below, change it as needed:
        $fields['author'] = $author_field;
        $fields['email'] = $email_field;
        $fields['url'] = $url_field;
        $fields['comment'] = $comment_field;
        $fields['cookies'] = $cookies_field;
        $fields['wp-comment-cookies-consent'] = '';
        // done ordering, now return the fields:
        return $fields;
    }

    add_filter( 'comment_form_fields', 'mo_comment_fields_custom_order' );
 }

 

 