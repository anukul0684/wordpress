<?php

/**
 * Index template for ChopShop
 */

 get_header();

?>  <div id="content">
		<div id="primary" class="single">

			<article>

				<h1 class="archive_title">Recent Posts</h1>

				<!-- run a loop to list the posts if added through editor -->
				<?php while(have_posts()) : the_post(); ?>

					<!-- add permalink for each to visit the single detail page -->
					<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

					<!-- add name of the author and the date on which the post was created -->
					<p><small>Posted by <?php the_author() ?> on <?php the_date() ?></small></p>

					<!-- brief exhibit of the post content -->
					<div class="content">
						<?php the_excerpt(); ?>
					</div>
					
				<?php endwhile; ?>
				
			</article>
			<!-- #post-## -->
			
		</div><!-- /primary -->
		
		<?php get_sidebar(); ?>

	</div><!-- /content -->

<?php 

    get_footer();

?>