<div id="secondary">
    <!-- for adding search box form on sidebar -->
    <?=get_search_form();?>

    <!-- for adding pages of the website and here in particular using Menu -->
    <h3>Menu</h3>

    <div class="menu">
        <ul class="menu">                            
            <?php wp_nav_menu(['menu' => 'main']) ?>            
        </ul>        
    </div>
    
</div><!-- /secondary -->
