<div id="secondary">
    <!-- added search form for user inputs -->
    <?=get_search_form();?>


    <!-- main menu added for pages selected in editor -->
    <h3>Menu</h3>

    <div class="menu">
        <ul class="menu">                            
            <?php wp_nav_menu(['menu' => 'main']) ?>            
        </ul>        
    </div>

    <!-- list of categories in that has posts -->
    <div class="categories">
        <h3>Categories</h3>
        <ul class="menu">
            <?php wp_list_categories(['title_li' => '', 'depth' => 1]); ?>
        </ul>
    </div>

    <!-- list of months with year that have posts  -->
    <div class="archives">
        <h3>Archive</h3>
        <ul class="menu">
            <?php wp_get_archives() ?>
        </ul>
    </div>   
    
</div><!-- /secondary -->
