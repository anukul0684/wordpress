<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'chopshop' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HK@:&zP[1vPDpz_Avq<;i+45+>9oH}b+rQWph%V8E]1HchBmq-|EpJO79V]j|ER6');
define('SECURE_AUTH_KEY',  'p6g+uiLaJ;IaMZ<~%bVFm;IdU$+d6d9|y=MWcE|k,OMVk9Z/?w(H[2VF|e.7[r3_');
define('LOGGED_IN_KEY',    'y8VUNU|ImEC:[2j9aTw,3U07N(y9L}&,W.Xez5w[a$FDTL^!oEXi!-]LHt=?bD#I');
define('NONCE_KEY',        '_acW=),b|Vd4uHYk{SoVUzULof~w _>d9|.tt:Q-bQSuOo|idhd0Z6I#==on<Gu)');
define('AUTH_SALT',        'TLP|lAOF=7-?ze=S}-}%]alF0!,4kJ y]V^U%>eHY+bI*s-uXv0<%nSwS;t52yzw');
define('SECURE_AUTH_SALT', '#He-^)er@loA73sWwT89`l_+{^(a{w{g0.RN[}-:4>_w@AABt3zOFS4xUU)C*k|y');
define('LOGGED_IN_SALT',   '~it-qF%?mV3f)Ap<HT9=[|RK`<s:VO3|P,>+-$Ktt:v$+3 S)x?Nf%b),KZ8>IPh');
define('NONCE_SALT',       'TE(y?M{tP- 4wYmD1FVNrcn?VE<@6~pO-zT;P&NC;+DYmu/qA#x@UYQuM9*zdg(+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
